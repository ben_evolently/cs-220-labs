/* 
 * CS:APP Data Lab 
 * 
 * <Bienvenido Rodriguez brodrig4>
 * 
 * bits.c - Source file with your solutions to the Lab.
 *          This is the file you will hand in to your instructor.
 *
 * WARNING: Do not include the <stdio.h> header; it confuses the dlc
 * compiler. You can still use printf for debugging without including
 * <stdio.h>, although you might get a compiler warning. In general,
 * it's not good practice to ignore compiler warnings, but in this
 * case it's OK.  
 */

#if 0
/*
 * Instructions to Students:
 *
 * STEP 1: Read the following instructions carefully.
 */

You will provide your solution to the Data Lab by
editing the collection of functions in this source file.

INTEGER CODING RULES:
 
  Replace the "return" statement in each function with one
  or more lines of C code that implements the function. Your code 
  must conform to the following style:
 
  int Funct(arg1, arg2, ...) {
      /* brief description of how your implementation works */
      int var1 = Expr1;
      ...
      int varM = ExprM;

      varJ = ExprJ;
      ...
      varN = ExprN;
      return ExprR;
  }

  Each "Expr" is an expression using ONLY the following:
  1. Integer constants 0 through 255 (0xFF), inclusive. You are
      not allowed to use big constants such as 0xffffffff.
  2. Function arguments and local variables (no global variables).
  3. Unary integer operations ! ~
  4. Binary integer operations & ^ | + << >>
    
  Some of the problems restrict the set of allowed operators even further.
  Each "Expr" may consist of multiple operators. You are not restricted to
  one operator per line.

  You are expressly forbidden to:
  1. Use any control constructs such as if, do, while, for, switch, etc.
  2. Define or use any macros.
  3. Define any additional functions in this file.
  4. Call any functions.
  5. Use any other operations, such as &&, ||, -, or ?:
  6. Use any form of casting.
  7. Use any data type other than int.  This implies that you
     cannot use arrays, structs, or unions.

 
  You may assume that your machine:
  1. Uses 2s complement, 32-bit representations of integers.
  2. Performs right shifts arithmetically.
  3. Has unpredictable behavior when shifting an integer by more
     than the word size.

EXAMPLES OF ACCEPTABLE CODING STYLE:
  /*
   * pow2plus1 - returns 2^x + 1, where 0 <= x <= 31
   */
  int pow2plus1(int x) {
     /* exploit ability of shifts to compute powers of 2 */
     return (1 << x) + 1;
  }

  /*
   * pow2plus4 - returns 2^x + 4, where 0 <= x <= 31
   */
  int pow2plus4(int x) {
     /* exploit ability of shifts to compute powers of 2 */
     int result = (1 << x);
     result += 4;
     return result;
  }

FLOATING POINT CODING RULES

For the problems that require you to implent floating-point operations,
the coding rules are less strict.  You are allowed to use looping and
conditional control.  You are allowed to use both ints and unsigneds.
You can use arbitrary integer and unsigned constants.

You are expressly forbidden to:
  1. Define or use any macros.
  2. Define any additional functions in this file.
  3. Call any functions.
  4. Use any form of casting.
  5. Use any data type other than int or unsigned.  This means that you
     cannot use arrays, structs, or unions.
  6. Use any floating point data types, operations, or constants.


NOTES:
  1. Use the dlc (data lab checker) compiler (described in the handout) to 
     check the legality of your solutions.
  2. Each function has a maximum number of operators (! ~ & ^ | + << >>)
     that you are allowed to use for your implementation of the function. 
     The max operator count is checked by dlc. Note that '=' is not 
     counted; you may use as many of these as you want without penalty.
  3. Use the btest test harness to check your functions for correctness.
  4. Use the BDD checker to formally verify your functions
  5. The maximum number of ops for each function is given in the
     header comment for each function. If there are any inconsistencies 
     between the maximum ops in the writeup and in this file, consider
     this file the authoritative source.

/*
 * STEP 2: Modify the following functions according the coding rules.
 * 
 *   IMPORTANT. TO AVOID GRADING SURPRISES:
 *   1. Use the dlc compiler to check that your solutions conform
 *      to the coding rules.
 *   2. Use the BDD checker to formally verify that your solutions produce 
 *      the correct answers.
 */


#endif
/* 
 * bitNor - ~(x|y) using only ~ and & 
 *   Example: bitNor(0x6, 0x5) = 0xFFFFFFF8
 *   Legal ops: ~ &
 *   Max ops: 8
 *   Rating: 1
 */
int bitNor(int x, int y) {
       return ~(x) & ~(y); //NOR at an input of 0,0 returns 1, so and the complement of x and and complement y
}
/* 
 * thirdBits - return word with every third bit (starting from the LSB) set to 1
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 8
 *   Rating: 1
 */
int thirdBits(void) {
  int pattern = 0x49; // 0100 1001
  pattern |= (pattern << 9); // Pattern repeats every 9 bits, so shift it 9 bits.
  pattern |= (pattern << 18); // Shift it 18 bits.

  return pattern; // Pattern now covers all 32 bits.
}
/* 
 * copyLSB - set all bits of result to least significant bit of x
 *   Example: copyLSB(5) = 0xFFFFFFFF, copyLSB(6) = 0x00000000
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 5
 *   Rating: 2
 */
int copyLSB(int x) {
  return (((x) <<  31) >> 31); // exploiting arithmetic shift
}
/* 
 * replaceByte(x,n,c) - Replace byte n in x with c
 *   Bytes numbered from 0 (LSB) to 3 (MSB)
 *   Examples: replaceByte(0x12345678,1,0xab) = 0x1234ab78
 *   You can assume 0 <= n <= 3 and 0 <= c <= 255
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 10
 *   Rating: 3
 */
int replaceByte(int x, int n, int c) {
  int shift = n << 3; // shift n left by 3 (multiplying n by 8)
  int mask = 0xFF << shift; // shift constant by shift (real mask)
  int correctSpot = c << shift; // shift byte to correct spot
  return (x & ~mask) | correctSpot; // clear byte and replace with mask
}
/* 
 * logicalShift - shift x to the right by n, using a logical shift
 *   Can assume that 0 <= n <= 31
 *   Examples: logicalShift(0x87654321,4) = 0x08765432
 *   Legal ops: ~ & ^ | + << >>
 *   Max ops: 20
 *   Rating: 3 
 */
int logicalShift(int x, int n) {
  int retrieveAndClear = (0xFF << 31) >> n; // shift constant 31 bits to left to retrieve significant bit and shift that by n to clear arithmetic shifted bits
  int mask = ~(retrieveAndClear << 1); // negate it and shift by 1 (multiply by 2) to create a mask that keeps the logical data
  return (x >> n) & mask; // shifts and applies mask
}
/* 
 * greatestBitPos - return a mask that marks the position of the
 *               most significant 1 bit. If x == 0, return 0
 *   Example: greatestBitPos(96) = 0x40
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 70
 *   Rating: 4 
 */
int greatestBitPos(int x) {
  // HOW THIS WORKS:
  // Everytime we | the value with itself shifted to the right, we duplicate the 1's to the RIGHT.
  // We increase the amount we shift in powers of 2 to continue duplicating.

  // Now, the most significant will stay constant b/c by definition there won't be any 1's to the left of it. Thus
  // everything to the left will remain 0, and eventually, everything to the right will be filled with 1's. We then
  // ^ this value with the value shifted to the right by 1, placing a 0 in the GBP and 1's everywhere else which will
  // clear the extra bits for us.
  int shiftedX;
  int Tmax = ~(1 << 31);
  shiftedX = x >> 1;
  x |= (shiftedX) & Tmax;
  shiftedX = x >> 2;
  x |= (shiftedX) & (Tmax >> 1);
  shiftedX = x >> 4;
  x |= (shiftedX) & (Tmax >> 3);
  shiftedX = x >> 8;
  x |= (shiftedX) & (Tmax >> 7);
  shiftedX = x >> 16;
  x |= (shiftedX) & (Tmax >> 15);
  shiftedX = x >> 1;
  return (x ^ shiftedX & Tmax); // To make sure that we don't get trouble with arithmetic shift, we'll AND our value with
  // TMax because the highest value we'll ever deal with will be all 1's (Our goal is to erase all bits after most
  // significant bit, and so since the worst case scenario is 1111...1111, TMax (0111...1111) will get rid of the bits in case
  // arithematic shift tries to misbehave)
}
/*
 * isTmax - returns 1 if x is the maximum, two's complement number,
 *     and 0 otherwise 
 *   Legal ops: ! ~ & ^ | +
 *   Max ops: 10
 *   Rating: 1
 */
int isTmax(int x) {
  int mask1 = x + 1; // if x = Tmax, if we add 1 to it, it becomes Tmin
  int mask2 = !(mask1); // we bang value coming out above, if x = Tmax, then this will return 0
  int mask3 = ~(x + mask1); // add value to mask1, if x = Tmax, then Tmax + Tmin = -1 and complement returns 0s
  return !(mask3 | mask2); // if x = Tmax, !(0|0) returns 1 all else returns 0
}
/*
 * ezThreeFourths - multiplies by 3/4 rounding toward 0,
 *   Should exactly duplicate effect of C expression (x*3/4),
 *   including overflow behavior.
 *   Examples: ezThreeFourths(11) = 8
 *             ezThreeFourths(-9) = -6
 *             ezThreeFourths(1073741824) = -268435456 (overflow)
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 12
 *   Rating: 3
 */
int ezThreeFourths(int x) {
  int multiplyThree = (x << 1) + x; // multiples by 2 adds x to have final value of 3x
  int checkNeg = multiplyThree >> 31; // shift 31 to retrieve MSB - check if x is negative
  int storeBits = checkNeg & 0x3; // store bits about to be lost if number is negative
  int correctRound = multiplyThree + storeBits; // check if it rounds correctly when added
  return correctRound >> 2; //divide by 4 and return
}
/* 
 * isAsciiDigit - return 1 if 0x30 <= x <= 0x39 (ASCII codes for characters '0' to '9')
 *   Example: isAsciiDigit(0x35) = 1.
 *            isAsciiDigit(0x3a) = 0.
 *            isAsciiDigit(0x05) = 0.
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 15
 *   Rating: 3
 */
int isAsciiDigit(int x) {
  int greaterThanThirty = x + (~0x30 + 1); // This checks if greater than 30
  int lessThanThirtyNine = 0x39 + (~x + 1); // This checks if less than 39, so first two lines together checks 39 =< x <= 30;
  int range = greaterThanThirty | lessThanThirtyNine; // OR both to see if it's outside range of 30 and 39
  return !(range >> 31); // right shift to obtain MSB and bang
}
/* 
 * isGreater - if x > y  then return 1, else return 0 
 *   Example: isGreater(4,5) = 0, isGreater(5,4) = 1
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 24
 *   Rating: 3
 */
int isGreater(int x, int y) {
  int controlOpposite = x^y; // Will have 1 as MSB when x and y have opposite sign, so we'll use this as a control.
  int controlSame = ~controlOpposite; // Will have 1 as MSB when x and y have same sign, we'll use this as the opposite of the above.
  int checkPosNeg = y + ~x + 1; // Will have 1 as MSB when (y - x) is negative (i.e. x > y).
  int sameSign = checkPosNeg & controlSame; // When the signs are the same, we'll want to return our difference
  int differentSign = y & controlOpposite; // When signs are different, we'll want to return if x is +, y is - as 1, and x is -, y is + as 0.
  int comparison = sameSign | differentSign; // Comparison between controls
  int getMSB = comparison >> 31; // Get MSB, & it with 1, and that's our final value.
  return (getMSB & 1);
}
/* 
 * isNonZero - Check whether x is nonzero using
 *              the legal operators except !
 *   Examples: isNonZero(3) = 1, isNonZero(0) = 0
 *   Legal ops: ~ & ^ | + << >>
 *   Max ops: 10
 *   Rating: 4 
 */
int isNonZero(int x) {
  int OrWithTwosC = x | (~x + 1); // OR with 2's complement (x is only the same with 0)
  int MSB = OrWithTwosC >> 31; // when OR with 2's complement we get all 1s unless it's zero
  return MSB & 1; // AND with 1 to return 1 or 0 
}
/* 
 * sm2tc - Convert from sign-magnitude to two's complement
 *   where the MSB is the sign bit
 *   Example: sm2tc(0x80000005) = -5.
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 15
 *   Rating: 4
 */
int sm2tc(int x) {
  int checkNeg = x >> 31; // right shift 31 to obtain MSB to see if negative
  int simFlip = x ^ checkNeg; // simulates flipping bits, if negative changes to 1s complement otherwise left alone
  int addOne = (((1 << 31) + 1) & checkNeg); // reset sign bit and if negative, add 1.
  return simFlip + addOne; // combine all to get 2's complement when necessary
}
/* 
 * float_abs - Return bit-level equivalent of absolute value of f for
 *   floating point argument f.
 *   Both the argument and result are passed as unsigned int's, but
 *   they are to be interpreted as the bit-level representations of
 *   single-precision floating point values.
 *   When argument is NaN, return argument..
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 10
 *   Rating: 2
 */
unsigned float_abs(unsigned uf) {
  return uf & 0x7FFFFFFF; // get rid of positive bit with Tmax
}
/* 
 * float_half - Return bit-level equivalent of expression 0.5*f for
 *   floating point argument f.
 *   Both the argument and result are passed as unsigned int's, but
 *   they are to be interpreted as the bit-level representation of
 *   single-precision floating point values.
 *   When argument is NaN, return argument
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 30
 *   Rating: 4
 */

unsigned float_half(unsigned uf) {
  int retVal; // return value
  int exponentBits = uf & 0x7F800000; // isolates exponent bits
  int signBit = uf & 0x80000000; // isolates sign bits
  int mantissaBits = uf & 0x007FFFFF; // isolates mantissa bits
  int lostLSB = uf & 1; // Keep track of LSB for rounding (this is the bit that will be lost when we do our division)
  int newLSB; // New LSB used for rounding when in denormalized

  switch(exponentBits) // checks normalized, denormalized and nan
    {
    case 0x7F800000: // when the value is NaN
      retVal = uf; // do nothing and return to user
      break;

    case 0x00800000: // When the value is going to move from normalized to denormalized
    case 0x00000000: // When the value IS denormalized
	mantissaBits>>= 1; // Divide the mantissa by 2
	newLSB = mantissaBits & 1; // Find the new LSB in the mantissa, which will be used for rounding
	mantissaBits += lostLSB & newLSB;// When the previous LSB and the current LSB are BOTH 1, round upwards, otherwise round down
	retVal = (exponentBits >> 1) | mantissaBits;// Shift the exp (in case this is going from normalized to denormalized) and apply it to the new mantissa.
      break;

    default: // normalized
      retVal = (exponentBits - 0x00800000) | mantissaBits; // Subtract 1 from exp, which divides the value in half.
      break;
    }
  return retVal | signBit; // place sign bit back into the value and return.
  }
/* 
 * float_i2f - Return bit-level equivalent of expression (float) x
 *   Result is returned as unsigned int, but
 *   it is to be interpreted as the bit-level representation of a
 *   single-precision floating point values.
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 30
 *   Rating: 4
 */
/* 
 * float_i2f - Return bit-level equivalent of expression (float) x
 *   Result is returned as unsigned int, but
 *   it is to be interpreted as the bit-level representation of a
 *   single-precision floating point values.
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 30
 *   Rating: 4
 */
unsigned float_i2f(int x) {
  int exponentBits = 0; 
  int mantissaBits = 0;
  int signBit = 0; 
  int lastBit = 0; // Used to store where we found the MSB 1 bit in the variable x
  int i = 1; // iterator 
  int decrementor; // Used for trying to find the MSB 1 bit in variable x
  int magnitude = x; // check the magnitude of variable x
  //int retVal = 0;

  if(x == 0) // When x is 0, we can return 0.
    {
      return 0;
      //retVal = 0;
    }
  if(x == 0x80000000) // TMin behaves poorly so brute force its value when requested.
    {
      return 0xcf000000;  // Compiler yelled at me for Tmin, so I returned this.
      //retVal = 0xcf000000;
    }
  if(x < 0) // If x is negative...
    {
      signBit = 0x80000000; // Set the sign bit to negative.
      magnitude = -magnitude; // Change magnitude into a positive value.
    }

  i = 0x80000000; // Begin our search at the MSB of our int.
  for (decrementor = 31; decrementor >= 0; decrementor--) // Loop through bits and find MS 1 bit.
    {
      if(magnitude & i) // If we find a 1 at the bit we're currently looking at...
	{
	  lastBit = decrementor; // We've found the position where the MS 1 is.
          break; // Break when we find the first 1.
	}
      i = i >> 1; // Shift bit to right to check next bit.
    }

  exponentBits = lastBit + 127; // lastBit will be our E value in the equation: EXP - BIAS = E.
  magnitude<<= (31 - lastBit); // Now that we know our E value, we can get rid of EXP and SIGN bits and focus on mantissa, so we get rid of them by shifting.
  mantissaBits = ((magnitude >> 8) & 0x007FFFFF); // The mantissa is now our 23 MSBits, so we'll find them and have them set to a variable using a mask.
  magnitude = (magnitude & 0xFF); // Save the bits that we're losing.

  // At first, attempted to use Guard bits to determine rounding, but it wouldn't work. So went with this attempt instead. Not entirely sure when the 
  // Guard bits become relevent.

  if (magnitude > 128 || (magnitude == 128 && mantissaBits & 1)) // If we're losing over 1/2 of a mantissa bit, OR if the mantissa's odd and we have exactly 1/2...
    {
      mantissaBits++; // Round up
    }

  if (mantissaBits >> 23) // If mantissa has overflowed...
    {
      mantissaBits = 0; // Clear the mantissa.
      exponentBits++; // Increment exp b/c we have a number too large for mantissa to handle.
    }
  //retVal = signBit | (exp << 23) | mantissa;
  
 return signBit | (exponentBits << 23) | mantissaBits; // Combine the 3 parts of a IEEE floating point number and return.
}
