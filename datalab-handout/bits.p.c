#line 141 "bits.c"
int bitNor(int x, int y) {
       return ~(x) & ~(y);
}
#line 150
int thirdBits(void) {
  int mask1=( 0x2 << 8) | 0x49;
  int mask2=  mask1 << 12 | mask1;
  return mask2 |( mask2 << 24);

}
#line 163
int copyLSB(int x) {
  return (((x) << 31) >> 31);
}
#line 175
int replaceByte(int x, int n, int c) {
  int mask1=  n << 3;
  int mask2=  0xFF << mask1;
  int mask3=  c << mask1;
  return (x & ~mask2) | mask3;
}
#line 189
int logicalShift(int x, int n) {
  int mask1=(  0xFF << 31) >> n;
  int mask2=  ~(mask1 << 1);
  return (x >> n) & mask2;
}
#line 202
int greatestBitPos(int x) {
  int max=  ~(1 << 31);
  x |=( x >> 1) & max;
  x |=( x >> 2) &( max >> 1);
  x |=( x >> 4) &( max >> 3);
  x |=( x >> 8) &( max >> 7);
  x |=( x >> 16) &( max >> 15);
  return (x& ~(x >> 1 & max));

}
#line 219
int isTmax(int x) {
  int mask1=  x + 1;
  int mask2=  !(mask1);
  int mask3=  ~(x + mask1);
  return !(mask3 | mask2);
}
#line 236
int ezThreeFourths(int x) {
  int mask1=(  x << 1) + x;
  int mask2=  mask1 >> 31;
  int mask3=  mask2 & 0x3;
  int mask4=  mask1 + mask3;
  return mask4 >> 2;
}
#line 252
int isAsciiDigit(int x) {
  int mask1=  x +( ~0x30 + 1);
  int mask2=  0x39 +( ~x + 1);
  int mask3=  mask1 | mask2;
  return !(mask3 >> 31);
}
#line 265
int isGreater(int x, int y) {
int mask0=  x^y;
int mask1=  ~mask0;
int mask2=  y + ~x + 1;
int mask3=  mask1 & mask2;
int mask4=  y & mask0;
int mask5=  mask3 | mask4;
int mask6=  mask5 >> 31;
  return !!(mask6);
}
#line 283
int isNonZero(int x) {
  int mask1=  x |( ~x + 1);
  int mask2=  mask1 >> 31;
  return mask2 & 1;
}
#line 296
int sm2tc(int x) {
int mask1=  x >> 31;
 int mask2=  x ^ mask1;
 int mask3=(((  1 << 31) + 1) & mask1);
 return mask2 + mask3;
}
#line 313
unsigned float_abs(unsigned uf) {
  return uf & 0x7FFFFFFF;
}
#line 327
unsigned float_half(unsigned uf) {
  int retVal;
  int mask1=  uf & 0x7F800000;
  int mask2=  uf & 0x80000000;
  int mask3=  uf & 0x007FFFFF;
  int mask4=  uf & 1;
  int mask5;

  switch (mask1) 
    {
    case 0x7F800000: 
      retVal = uf;
      break;

    case 0x00800000: 
    case 0x00000000: 
 mask3>>= 1;
 mask5 = mask3 & 1;
 mask3+= mask4 & mask5;
 retVal =( mask1 >> 1) | mask3;
      break;

    default: 
      retVal =( mask1 - 0x00800000) | mask3;
      break;
    }
  return retVal | mask2;
}
#line 364
unsigned float_i2f(int x) {
  unsigned     mask=  0xFFFFFFFF;
  int exp=  0;
  int mantissa=  0;
  int signBit=  0;
  int lastBit=  0;
  int incrementor=  0;
  int i=  1;
  int magnitude=  x;
  int guardBits=  0;
  signBit = 0;
  if (x== 0) 
    {
      return 0;
    }
  if (x< 0) 
    {
      signBit =0x80000000;
      magnitude = -x;
    }
  while (incrementor< 32) 
    {
      if (!(magnitude& i) == 0) 
 {
   lastBit = incrementor;
 }
      i = i << 1;
      incrementor++;
    }
  exp = lastBit + 127;
  mantissa = magnitude &( mask >>( 32 - lastBit));

  if (lastBit> 25) 
    {
      mantissa >>=(lastBit-26);
    }
  else 
    {
      mantissa <<=(26-lastBit);
    }
  guardBits = mantissa & 7;
  mantissa >>=3;

  switch (guardBits) 
    {
    case 5: 
    case 6: 
    case 7: 
      mantissa++;
      break;
    }

  return signBit |( exp << 23) | mantissa;
}
