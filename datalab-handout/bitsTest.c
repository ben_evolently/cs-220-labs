/* 
 * float_i2f - Return bit-level equivalent of expression (float) x
 *   Result is returned as unsigned int, but
 *   it is to be interpreted as the bit-level representation of a
 *   single-precision floating point values.
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 30
 *   Rating: 4
 */
unsigned float_i2f(int x) {
  unsigned res = x & (1 << 31); /* set sign bit */
  int i;
  int exp = (x >> 31) ? 158 : 0; /* 158 = 31 + 127, INT_MIN or zero */
  int frac = 0;
  int delta;
  int frac_mask;

  if (x << 1) { /* x is neither 0 nor INT_MIN */
  	if (x < 0)
      x = -x;
    i = 30;
    while ( !((x >> i) & 1) ) /* low 31 bits are always have 1(s) */
      i--;
    exp = i + 127;
    x = x << (31 - i);
    frac_mask = (1 << 23) - 1;
    frac = frac_mask & (x >> 8);
    x = x & 0xff;
    delta = x > 128 || ((x == 128) && (frac & 1));
    frac += delta;
    if(frac >> 23) {
      frac &= frac_mask;
      exp += 1;
    }
  }
  res = res | (exp << 23);
  res = res | frac;
  return res;
}

