/* lab2 Starter
   Functionality: 
     declares a char **array (a dynamically allocated array of pointers)
     mallocs he char **array in a function (passing it in via ***)
     passes the char **array to function for load and print
*/

/*
assume input file:

horse
cat
rat
oryx
bat
alligator
crocodile
pig
monkey
aardvark
*/

#include <stdlib.h>
#include <stdio_ext.h>
#include <stdio.h>
#include <string.h>

#define  WORD_LENGTH  30
#define  INITIAL_ARRAY_MAX 50

typedef char* String; /*creating typedef for char* to make it easier to read */
typedef String* Dictionary; /* creating typedef for String* or char** to make it easier to read */

int loadArray(String inFileName, Dictionary *array, int *count, int *capacity);
int insertWord(Dictionary *dictionary, int *wordCount, String word, int *capacity);
void printArray(Dictionary array, int count);
void menu(Dictionary *array, int *count, int *capacity);
void increaseCapacity(Dictionary *array, int *capacity);
void printWordCount(int count);
int searchForWord(Dictionary array, String word, int count);
void removeWord(Dictionary array, int position, int *count);
void helpFunction();
void printToFile(Dictionary array, String outFileName, int count);

int main(int argc, char* argv[])
{

  Dictionary wordArray;  /* declare the dynamic array of strings */
  /* the array is NOT allocated yet */
  int capacity = INITIAL_ARRAY_MAX;
  int wordCount = 0;
  
  if (argc != 3)
    {
      fprintf(stderr,"Usage: %s inputfile outputfile\n", argv[0]);
      return 1;
    }
  
  if (loadArray(argv[1], &wordArray, &wordCount, &capacity) != 0)
    {
      fprintf(stderr,"    loadArray failed! Program terminating...\n");
      return 1;  /* don't need to call exit - we're in main! */
    }
  
  printf("\n  Finished loading %d words.\n", wordCount);
  
  /* HERE IS WHAT OUR ARRAY LOOKS LIKE NOW
     wordArray is pointing to a 50-element block of memory
     
     wordArray[0] is a char * -> "horse\0"
             [1] is a char * -> "cat\0"
             [2] is a char * -> "rat\0"
             [3] is a char * -> "oryx\0"
             ...      char * -> "bat\0"
             ...      char * -> "alligator\0"
             ...      char * -> "crocodile\0"
             ...      char * -> "pig\0"
             ...      char * -> "monkey\0"
             [9] is a char * -> "aardvark\0"
	     [10] is a char * -> ????
             ...
             ...
             ...
	     [49] is a char * -> ????
	     
	     REMEMBER the 50 char * pointers are stored consecutively in 1 block
	     of memory (which is why wordArray[5], for example, is well-defined,
	     but the strings are distributed "randomly" throughout the heap.
  */
  menu(&wordArray, &wordCount, &capacity);
  printToFile(wordArray, argv[2], wordCount);
  return 0;
}


int loadArray(String inFileName, Dictionary *array, int *count, int *capacity)
{
  FILE *inFile;
  char word[WORD_LENGTH];  /* this is the ONLY auto array we'll need */

  if ((inFile = fopen(inFileName, "r")) == NULL)
    {
      fprintf(stderr,"Error opening input file, %s\n", inFileName);
      return -1;
    }
  
  *array = (Dictionary)malloc(*capacity * sizeof(String));
  if (*array == NULL)
    {
      fprintf(stderr, "Malloc of array in loadArray failed!\n");
      return -1;
    }
  
  printf("Reading file %s (each . is 1000 words read)\n", inFileName);
  
  *count = 0;
  while (fscanf(inFile, "%s", word) == 1)
    {
      if (insertWord(array, count, word, capacity) != 0)
	{
	  fprintf(stderr,"    Insert returned an error!\n");
	  fclose(inFile);
	  return 1;
	}
	
      if (*count % 1000 == 0)
	{
	    printf(".");
	    fflush(stdout);  /*stdout is buffered, so have to force flush*/
	}
      
    }
  
  fclose(inFile);
  
  return 0;
}

int insertWord(Dictionary *array, int *count, String word, int *capacity)
{
  String wordPtr;
  int i = 0;
  int j;

  if (*count == *capacity)
    {
      increaseCapacity(array, capacity);
    }
  
  wordPtr = (String)malloc((strlen(word) + 1) * sizeof(char));
    if (wordPtr == NULL)
      {
	fprintf(stderr,"    Malloc of array[%d] failed!\n", *count);
	return -1;
      }
    /* Memory for this word has been allocated, so copy characters
       and insert into array */

    strcpy(wordPtr, word);

    /* Here's a visual of how strcmp will work:
    [ ][ ][x][ ][ ][ ]
    We want to enter our word at the 'x' denoted above
    This is after we have found the appropriate place to enter our word
    [ ][ ][x][j-1][j-1][j-1] */

    for(i = 0; i < *count; i++) /* we are going to start comparing words here using a for loop to be able to put the words in lexical order */
      {
	if(strcmp(word, (*array)[i]) == 0)
	  {
	    fprintf(stderr, "The word you entered already exists within the dictionary.\n");
	    return -1;
	  }
	if(strcmp(word, (*array)[i]) < 0) /* so, if we compare the two words and word entered is less than the array ith element we are at (the word at that ith element) */
	  break; /* we have found the point where we need to insert our word and we break */
      }

    for(j = *count; j > i; j--) /* this for loop starts from the last element because if we start from the first element we are just copying the word over and over and not moving anything over */
      /* so we start at j = *count, and we decrement */
      {
	(*array)[j] = (*array)[j-1]; /* this is where we shift all the elements in the array after the specific point we found to the right.*/
      }

   (*array)[i] = wordPtr;
   (*count)++;
    return 0;
}

/* function to increase array capacity */
void increaseCapacity(Dictionary *array, int *capacity)
{
  Dictionary tempArray; /* declaring a temporary array to copy into current array so we can increase size of our array */
  int i;
  tempArray = (Dictionary)malloc(2 * (*capacity) * sizeof(String)); /* allocate space using malloc for doubling the size of the array using (2 * (*capacity) */
  for(i = 0; i < *capacity; i++) /* for loop to iterator through temporary array and our array */
    {
      tempArray[i] = (*array)[i]; /* set everything in temp array's ith positions to array's ith position */
    }
  free(*array); /* free our array from its current address in memory */
  (*capacity) = (*capacity)*2; /* update capacity */ 
  *array = tempArray; /*update our array to equal what we have in our temparray.*/
}


/* print array function */
void printArray(Dictionary array, int count)
{
  int i; 
  for (i = 0; i < count; i++) /* for loop to iterate through dictionary */
    {
      printf("array[%d]: %s\n", i, array[i]); /* print out dictionary */
    }
}

/* print to File function */
void printToFile(Dictionary array, String outFileName, int count)
{
  FILE *outFile; int i;
  if ((outFile = fopen(outFileName, "w")) != NULL)
    {
      for(i = 0; i < count; i++) /* for loop to iterate through dictionary*/
	{
	  fprintf(outFile, "array[%d]: %s\n", i, array[i]);
	}
	fclose(outFile);
    }
}

/* print word count function */
void printWordCount(int count)
{
  printf("There are %d words in the dictionary.\n", count);
}

/* search for word */
int searchForWord(Dictionary array, String word, int count)
{
  int low, high, mid, value, retVal, notFound; /* binary search, initializing low value (starting at top of array), high value (starting at end of array - 1) */
  low = 0;
  high = count - 1;
  notFound = 1;
  retVal = -1;

  while(notFound) /* while loop to go through cutting the dictionary in half over and over again until we find or don't find the word */
    {
      mid = (high + low)/2; /* cut in half */
      value = strcmp(word, array[mid]); /* compare word and the value at mid */
      if(value < 0)
	{
	  high = mid - 1; /*disregard mid to make it slightly faster*/
	}
      else if(value > 0)
	{
	  low = mid + 1; /*disregard mid to make it slightly faster*/
	}
      else
	{
	  notFound = 0;
	  retVal = mid;
	}

      if(mid == high) /* value not found, we exhausted through the dictionary */
	{
	  notFound = 0;
	}
    }

  return retVal;
}

/* remove word function */    
void removeWord(Dictionary array, int position, int *count)
{
  int i;
  free(array[position]); /* free position in the array where the word is */
  for(i = position; i < *count; i++)
    {
      array[i] = array[i+1]; /* iterate through array and set the values all equal to i + 1 to go over the empty spot that was freed */
    }
  (*count)--;
}

/* help function */
void helpFunction()
{
  printf("Search: Press S or S and Input a Word to Search for a Word Within Dictionary.\n");
  printf("Insert: Press I or i and Input a Word to Insert into Dictionary.\n");
  printf("Remove: Press R or r and Input a Word to Remove from Dictionary if it Exists in the Dictionary.\n");
  printf("Count: Press C or c to find the count of the Dictionary.\n");
  printf("Print: Press P or p to print out contents of Dictionary.\n");
  printf("Quit: Press Q or q to quit out of program and write the contents of Dictionary to an output File.\n");
}
  
	

/* menu function to display menu */
void menu(Dictionary *array, int *count, int *capacity)
{ 
  char option; /* option to be entered */
  char input[WORD_LENGTH]; /* input to be entered for the insert, search and remove options */
  int retVal;
  do /* do-while loop to first run the menu without checking anything and at the end check if 'Q' or 'q' */
    {
      printf("Welcome to the Dictionary!\n");
      printf("Choose the first letter of the option that will be presented below to intereact with the Dictionary.\n");
      printf("'S'earch\n");
      printf("'I'nsert\n");
      printf("'R'emove\n");
      printf("'C'ount\n");
      printf("'P'rint\n");
      printf("'Q'uit\n");
      printf("'H'elp\n");
      printf("Please, enter your option: \n");
      scanf("%c", &option); /* scan option */

      switch(option) /* switch statement to test for the option entered */
	{
	  /* case for print */
	case 'p':
	case 'P':
	  printArray(*array, *count);
	  break;
	  
	  /* case for search */
	case 's':
	case 'S':
	  printf("Please, input a word to be searched for in the Dictionary:\n");
	  scanf("%s", input); /* scan input */
	  printf("You are searching for the word: %s\n", input);
	  if (searchForWord(*array, input, *count) > -1) /* send appropriate values to search function to search for it */
	    {
	      printf("We found the word\n");
	    }
	  else
	    {
	      fprintf(stderr, "Word doesn't exist.\n");
	    }
	  
	  break;
	  
	  /* case for insert */
	case 'i':
	case 'I':
	  printf("Please, input a word to be inserted into the Dictionary:\n"); 
	  scanf("%s", input); /* scan input */
	  printf("You entered the word: %s\n", input);
	  if (insertWord(array, count, input, capacity) != 0) /* send appropriate values to insert function */
          {
		fprintf(stderr, "Error in insert word!\n");
	  }
	  break;

	  /* case for remove */
	case 'r':
        case 'R':
          printf("Please, input a word to be removed from the Dictionary:\n");
	  scanf("%s", input);
	  printf("Let's search for the word you inputed: %s\n", input);
	  retVal = searchForWord(*array, input, *count); /* send to search function to find the word if it exists */
	  if(retVal > -1)
	    
	    {
	      printf("Word exists........Now removing......\n");
	      removeWord(*array, retVal, count); /* now send the values to remove function to remove word */
	      printf("Remove word successful\n");
	    }
	  else
	    {
	      fprintf(stderr, "Word doesn't exist. I can't remove something that doesn't exist within the Dictionary.\n");
	    }
	      
	  break;

	  /* case for print word count */
	case 'c':
        case 'C':
          printWordCount(*count);
          break;
	  
	  /* case for quit */
	case 'q':
        case 'Q':
          printf("Thanks for checking out the Dictionary!\n");
	  printf("Now printing contents of Dictionary to output file.\n");
          break;

	  /* case for help */
	case 'h':
        case 'H':
          helpFunction();                                                                                                                                                                  
          break;
	
	/* if user entered wrong input */
	default:
	  fprintf(stderr, "You entered an invalid option\n");
	  break;
	}
      __fpurge(stdin); /* this was needed since my menu was printing out more than once sometimes */
    } while(option != 'q' && option != 'Q'); /* check if input entered was 'q' or 'Q' */
}
