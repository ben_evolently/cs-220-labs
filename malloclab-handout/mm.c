/**
 * This malloc lab implements a doubly linked list.
 * It uses the first fit searching algorithm, so when we search through our list
 * we find the first place our block will fit and place it in there.
 * Currently, this runs 45 util and 40 thru at a total grade of 85 (on remote 01)
 * This lab currently uses a relatively naive approach and is very simplisitic.
 * Linked lists are slow at searching at a complexity of O(n). 
 * Linked list inserts are also at a complexity of 0(n)
 * What we do is combine the inserting and searching and do it at once and we sort
 * The linked list in descending order by size
 * This is what makes the code so fast. When we call upon the findSpace function to
 * find us space, since our list is in descending order, if our block can not fit
 * at the largest space, which is the first node in the linked list, then there is no
 * use of searching the linked list with a loop
 * Instead, we just exit the function. We turned the search of a linked list into 
 * a complexity of 0(1)
 *
 * Ways we can make this malloc better and if I was a better, more experienced programmer:
 * (1)(Decrease Fragmentation but also Decrease Throughput):
 * Instead of using a find first fit searching algorithm, we can use a find best fit
 * searching algorithm. What this does, is find the smallest size free block that matches 
 * our block we want to insert and place it there. This will decrease fragmentation in our list
 * but the drawback is that it is time consuming to try to search for the best fit so our throughput
 * will suffer but we can combine it with the second idea down below to minimize the time.
 * (2)(Increase Throughput but Increase Fragmentation):
 * We can use a different data structure called a binary search tree. 
 * How it would work: It is basically the same idea as a linked list but it is a lot faster to search through
 * Its complexity time O(n log2n). How I would implement it and how I understand it: It would split the list in a way I organize 
 * the free list in my current implementation. From the root, the left subtree would be the smaller free blocks
 * and the right subtree would be the larger free blocks. So, when we search, we can have our cases to show what we need to 
 * insert into the list based on the block size. The problem with this is that with the minimum block size being 24 bytes, a request
 * to, for example, 1 byte will cost us 24 bytes. Which is definiely not space friendly. Another issue with space is that
 * The minimum 24 bytes will let every space smaller than 24 bytes become zombie because they can neither be allocated nor be freed as
 * long as one free block follows it.
 * So much fragmentation can occur and space utilization will suffer. 
 * (3)Solution to the top two is to combine them!
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include "mm.h"
#include "memlib.h"

team_t team = {
	"brodrig4",
	"Bienvenido Rodriguez",
	"brodrig4@binghamton.edu",
	"",
	""
};

/* Code from page 830 in the text */
/* Basic Constants and Macros */
#define WSIZE 4 // Word and header/footer size (bytes)
#define DSIZE 8 // Double word size (bytes)

#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) > (y) ? (x) : (y))

/* Requested block alignment */
#define ALIGNMENT 8

/* rounds up to the nearest multiple of ALIGNMENT */
#define ALIGN(p) (((size_t)(p) + (ALIGNMENT-1)) & ~0x7)

/* rounds up our minimum space to multiple of ALIGNMENT */
#define MINIMUM (ALIGN(24))

/* Merge size and alloc'd bit into a value */
#define EncodeSize(size, alloc) ((size) | (alloc))

/* Read and write values at address p */
#define ReadValAt(p) (*(size_t *)(p))
#define WriteValAt(p, val) (*(size_t *)(p) = (val))

/*Read size and the alloc'd values from address p */
#define ReadBlockSize(p) (ReadValAt(p) & ~0x7)
#define ReadBlockState(p) (ReadValAt(p) & 0x1) // If it's free or alloc'd

/*Given block ptr bp, compute address of next and previous blocks */
#define NextBlock(bp) ((void *)(bp) + ReadBlockSize(GetBlockHead(bp)))
#define PrevBlock(bp) ((void *)(bp) - ReadBlockSize(GetBlockHead(bp) - WSIZE))

/* Given block ptr bp, compute address of its header and footer */
#define GetBlockHead(bp) ((void *)(bp) - WSIZE)
#define GetBlockFoot(bp) ((void *)(bp) + ReadBlockSize(GetBlockHead(bp)) - DSIZE)

/* Given block ptr, compute address of next and previous FREE blocks */
#define NextFree(bp) (*(void **)(bp + DSIZE))
#define PrevFree(bp) (*(void **)(bp))

#define SIZE_T_SIZE (ALIGN(sizeof(size_t)))

/* Global variables that our code will use to manage its linked list */
static void *heapFreePtr = 0; // This is a pointer to the first free block of our linked list

/* Here we define the function prototypes that assist with the program */
static void *extendHeap(size_t word);
static void *findSpace(size_t adjustedSize);
static void *coalesce(void *blockPtr);
static void insertToFreeList(void *bp);
static void removeBlock(void *bp);
static void place(void *bp, size_t asize);

/**
 * mm_init - initialize the malloc package.
 *
 * Begin by setting up our "Free" linked list. The linked list will use a "dummy"
 * header to denote its end for simplicity. It's header will be set as "ALLOC'd"
 * for easy differentiation between free nodes and the end of the list.
 */
int mm_init(void)
{
	/* return -1 if unable to get heap space */
	void *bp;
	//void *mem sbrk(int incr): Expands the heap by incr bytes, where incr is a positive
	//non-zero integer and returns a generic pointer to the ﬁrst byte of the newly allocated heap area
	if ((bp = mem_sbrk(MINIMUM)) == NULL)
	{
		return -1;
	}

	/* Initialize the linked list by having the first node be an "end" block.
	End blocks are defined (in our code) to be an alloc'd block (1 encoded
	into its size) while still being in the FREE linked list (which should be
	filled by blocks with 0's encoded into size) */
	WriteValAt(GetBlockHead(bp), EncodeSize(MINIMUM, 1));
	WriteValAt(GetBlockFoot(bp), EncodeSize(MINIMUM, 1));
	heapFreePtr = bp;

	return 0;
}

/**
 * mm_malloc - Takes size/payload/what user uses/what data user gets and tries to find space for it
 * Always allocate a block whose size is a multiple of the alignment.
 * Relies on findSpace and proper linked list management for finding space. Also
 * does some simple math to keep our requests aligned - the max of the
 * minimum size (24 bytes) and the requested size (aligned size + 8)
 */
void *mm_malloc(size_t size)
{
	/* adjusted block size */
	size_t asize;
	char *bp;

	/* Ignore bogus requests */
	if (size <= 0)
		return NULL;

	/* Adjust block size to include overhead and alignment reqs and to be at least as big as our minimum */
	asize = MAX(ALIGN(size) + DSIZE, MINIMUM);

	/* Search the free list for a fit */
	if ((bp = findSpace(asize)))
	{
		/* place it at the mem location if found */
		place(bp, asize);
		return bp;
	}

	/* No fit found. Request more memory and place the block, or fail if it can't be done */
	if ((bp = extendHeap(asize)) == NULL)
		return NULL;

	/* place it at the mem location */
	place(bp, asize);
	return bp;
}

/**
 * mm_free - Freeing a block does nothing special, just adds it to a linked list for searching.
 */
 void  mm_free(void *bp)
{
	// In case we get a bogus request
	if (bp == NULL)
		return;

	// Reads size of the block being freed
	size_t size = ReadBlockSize(GetBlockHead(bp));

	// set header and footer to unallocated
	WriteValAt(GetBlockHead(bp), EncodeSize(size, 0));
	WriteValAt(GetBlockFoot(bp), EncodeSize(size, 0));
	//coalesce and add the block to the free list
	coalesce(bp);
}

/*
 * mm_realloc - Does some checks before alloc'ing a new spot
 * It either extends or shrinks an allocated block.
 * Explained in the code
 */
void *mm_realloc(void *ptr, size_t size)
{
	/* If this is a bogus request, mess with the caller :) */
	if (size <= 0)
	{
		mm_free(ptr);
		return NULL;
	}

	/* If oldptr is NULL, then this is just a malloc. */
	if (ptr == NULL)
	{
		return mm_malloc(size);
	}

	void *newptr;
	size_t newSize = MAX(ALIGN(size) + DSIZE, MINIMUM);
	size_t oldSize = ReadBlockSize(GetBlockHead(ptr));
	size_t copySize = MIN(newSize, oldSize);
	size_t nextSize = 0;
	int isNextFree = !ReadBlockState(GetBlockHead(NextBlock(ptr)));

	// If we can hijack some extra space to the right, do so
	if (isNextFree)
	{
		nextSize = ReadBlockSize(GetBlockHead(NextBlock(ptr)));
	}

	// Now check if we have the space to extend. If not, just call regular malloc.
	if (newSize <= oldSize + nextSize)
	{
		// Since we do, take the neighboring free out of our linked list b/c
		// we're going to change its size
		if (isNextFree)
		{
			removeBlock(NextBlock(ptr));
		}
		newptr = ptr;
		// So if we have space to SPLIT the total space into two
		if (oldSize + nextSize - newSize > MINIMUM)
		{
			// then do that! First define the new block's alloc'd space
			WriteValAt(GetBlockHead(newptr), EncodeSize(newSize, 1));
			WriteValAt(GetBlockFoot(newptr), EncodeSize(newSize, 1));
			// Then define the new free block's total size
			WriteValAt(GetBlockHead(NextBlock(newptr)), EncodeSize(oldSize+nextSize-newSize, 0));
			// And finally add it to the linked list of frees using mm_free
			mm_free(NextBlock(newptr));
		} else {
			//If we don't have enough space to split, we're going to just use the entire block we were found
			newSize = oldSize + nextSize;
			WriteValAt(GetBlockHead(newptr), EncodeSize(newSize, 1));
			WriteValAt(GetBlockFoot(newptr), EncodeSize(newSize, 1));
		}
	} else {
		newptr = mm_malloc(newSize);
		memcpy(newptr, ptr, copySize);
		if (newptr == NULL)
			return NULL;
		mm_free(ptr);
	}

	return newptr;
}


/* 
 * Coalesce - 
 * Joins adjecent free blocks together by 
 * finding the size of the new free block, then removing the
 * free block(s) from the free list, and fially changing the header
 * and footer information to the newly coalesced free block
 * 
 * A Visual: 
 * Join neighboring free blocks together to minimize our linked list size and ensure
 * malloc's have the most space to request from.
 * Four cases:
 * [ ALLOC'D ][ FREE ][ ALLOC'D ] == [ ALLOC'D ][ FREE ][ ALLOC'D ] ( Do nothing )
 * [ ALLOC'D ][ FREE ][  FREE   ] == [ ALLOC'D ][      FREE       ] ( extend right )
 * [  FREE   ][ FREE ][ ALLOC'D ] == [      FREE       ][ ALLOC'D ] ( extend left )
 * [  FREE   ][ FREE ][  FREE   ] == [           FREE             ] ( extend both )
*/
static void *coalesce(void *bp)
{
	size_t prev_alloc = 1;
	// If this is the start of our linked list, don't coalesce.
	if (PrevBlock(bp) == bp)
	{
		prev_alloc = 1;
	} else {
		// Otherwise, if our previous block has a FREE bit (0), it'll be looked at.
		prev_alloc = ReadBlockState(GetBlockFoot(PrevBlock(bp)));
	}
	size_t next_alloc = 1;
	if (NextBlock(bp))
	{
		// If our next block has a FREE bit (0), it'll be looked at.
		next_alloc = ReadBlockState(GetBlockHead(NextBlock(bp)));
	}
	// Take note of current block size
	size_t size = ReadBlockSize(GetBlockHead(bp));

	/* Case 1, extend the block rightward */
	if (prev_alloc && !next_alloc)
	{
		// Remove the right-block from the linked list b/c it's no longer
		// a single free block (it's joining this a new one)
		removeBlock(NextBlock(bp));
		// Update the block size to reflect the change
		size += ReadBlockSize(GetBlockHead(NextBlock(bp)));
		// Update the header/footer for block traversal
		WriteValAt(GetBlockHead(bp), EncodeSize(size, 0));
		WriteValAt(GetBlockFoot(bp), EncodeSize(size, 0));
	}

	/* Case 2, extend the block leftward */
	else if (!prev_alloc && next_alloc)
	{
		// Remove the left-block from the linked list b/c it's no longer
		// a single free block (it's joining this new one)
		removeBlock(PrevBlock(bp));
		// Update the block size to reflect the change
		size += ReadBlockSize(GetBlockHead(PrevBlock(bp)));
		// Update the header/footer for block traversal
		WriteValAt(GetBlockFoot(bp), EncodeSize(size, 0));
		WriteValAt(GetBlockHead(PrevBlock(bp)), EncodeSize(size, 0));
		// Since we moved leftwards our bp has shifted leftwards as well.
		bp = PrevBlock(bp);
	}

	/* Case 3, extend the block in both directions */
	else if (!prev_alloc && !next_alloc)
	{
		// Remove both left & right blocks from the linked list b/c they're
		// coalescing into one.
		removeBlock(PrevBlock(bp));
		removeBlock(NextBlock(bp));
		// Update block size to reflect the coalesence
		size += ReadBlockSize(GetBlockHead(PrevBlock(bp))) +
			ReadBlockSize(GetBlockHead(NextBlock(bp)));
		// Update the header/footer for block traversal
		WriteValAt(GetBlockHead(PrevBlock(bp)), EncodeSize(size, 0));
		WriteValAt(GetBlockFoot(NextBlock(bp)), EncodeSize(size, 0));
		// Since we moved leftwards to get the left free block, shift bp to reflect
		// that
		bp = PrevBlock(bp);
	}

	/* Case do nothing will reach here without trouble b/c it should do nothing. */

	// Finally insert our new free block into the linked list for searching.
	insertToFreeList(bp);
	return bp;
}

/* 
 * extendHeap -
 * Extend heap with free block and return its block pointer
 * This function maintains alignment by only allocating an even number of
 * words to extend the heap by.
 * Just extend the heap enough to create a new block 
 */

static void *extendHeap(size_t size)
{
	void *bp;
	if ((long)(bp = mem_sbrk(size)) == -1)
	{
		return NULL;
	}

	/* Initialize free block header/footer and the epilogue header */
	WriteValAt(GetBlockHead(bp), EncodeSize(size, 0)); /* Free block header */
	WriteValAt(GetBlockFoot(bp), EncodeSize(size, 0)); /* Free block footer */
	WriteValAt(GetBlockHead(NextBlock(bp)), EncodeSize(0, 1)); /* New Epilogue header */

	/* Coalesce if the previous block was free */
	return coalesce(bp);
}

/** findSpace-
 * Check the top of our linked list and return if it can be used.
 * 
 * Since our linked list is sorted from most space to least space,
 * we can instantly quit once we see that the first node can't fit our request.
 */
static void *findSpace(size_t adjustedSize)
{
	// 

	// If linked list is empty, there are no spaces to check
	if (ReadBlockState(GetBlockHead(heapFreePtr)) == 1)
		return NULL;

	// If the first block can't fit our request, none of the others can because we've sorted it in
	// descending order.
	if (adjustedSize <= (size_t)ReadBlockSize(GetBlockHead(heapFreePtr)))
		return heapFreePtr;

	return NULL;
}

// Places a block into a sorted linked list which is in descending order by size.
static void insertToFreeList(void *bp)
{
//	To optimize time we'll put the list into descending-order linked list by size.
	void *newPos = heapFreePtr;
	size_t size = ReadBlockSize(GetBlockHead(bp));
	// This loop will find the correct position in the list for the new block to go
	while (ReadBlockState(GetBlockHead(newPos)) == 0) // While reading a free block...
	{
		if (size >= (size_t)ReadBlockSize(GetBlockHead(newPos)))
		{
			break; // we found a spot to put the block in
		}
		newPos = NextFree(newPos);
	}

	// If this is the new biggest block, change our linked-list's head
	if (newPos == heapFreePtr)
	{
		NextFree(bp) = heapFreePtr; //Sets next ptr to start of free list
		PrevFree(bp) = NULL; // Sets prev pointer to NULL
		PrevFree(heapFreePtr) = bp; //Sets current's prev to new block
		heapFreePtr = bp; // Sets start of free list as new block
	} else {
	// Otherwise do normal insert linked-list stuff
		NextFree(bp) = newPos;
		PrevFree(bp) = PrevFree(newPos);
		NextFree(PrevFree(bp)) = bp;
		PrevFree(NextFree(bp)) = bp;
	}
}

/* removeBlock
 * Remove a block from a linked list.
*/
static void removeBlock(void *bp)
{
	// If there's a previous block, set its next pointer to the
	// next block.
	// If not, set the block's previous pointer to the prev block/
	if (PrevFree(bp))
	{
		NextFree(PrevFree(bp)) = NextFree(bp);
	} else {
		heapFreePtr = NextFree(bp);
	}

	PrevFree(NextFree(bp)) = PrevFree(bp);
}


// Does all the hard work of setting a raw block of memory to alloc'd space.
static void place(void *bp, size_t asize)
{
	//Get total size of the entire free block
	size_t totalBlockSize = ReadBlockSize(GetBlockHead(bp));


	//If the difference is at least 24 bytes...
	if ((totalBlockSize - asize) >= MINIMUM)
	{
	//change the header and footer for the allocated block (size = asize, allocated = 1)... 
		WriteValAt(GetBlockHead(bp), EncodeSize(asize, 1));
		WriteValAt(GetBlockFoot(bp), EncodeSize(asize, 1));
	//... and remove the block from the free list
		removeBlock(bp);

	//Now split the block by...
		bp = NextBlock(bp);

	//...Changing the header and footer info for the free block created from the
	//remaining space (size = totalBlockSize-asize, allocated = 0)
		WriteValAt(GetBlockHead(bp), EncodeSize(totalBlockSize-asize, 0));
		WriteValAt(GetBlockFoot(bp), EncodeSize(totalBlockSize-asize, 0));
	//Coalescing the new free block with adjacent free blocks
		coalesce(bp);

	//Else we change the header and footer based on the totalBlockSize
	} else {
		WriteValAt(GetBlockHead(bp), EncodeSize(totalBlockSize, 1));
		WriteValAt(GetBlockFoot(bp), EncodeSize(totalBlockSize, 1));
		removeBlock(bp);
	}
}
