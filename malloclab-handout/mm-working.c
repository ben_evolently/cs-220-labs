/*
* mm-naive.c - The fastest, least memory-efficient malloc package.
* 
* In this naive approach, a block is allocated by simply incrementing
* the brk pointer.  A block is pure payload. There are no headers or
* footers.  Blocks are never coalesced or reused. Realloc is
* implemented directly using mm_malloc and mm_free.
*
* NOTE TO STUDENTS: Replace this header comment with your own header
* comment that gives a high level description of your solution.
*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include "mm.h"
#include "memlib.h"

/*********************************************************
* NOTE TO STUDENTS: Before you do anything else, please
* provide your team information in the following struct.
********************************************************/
team_t team = {
	/* Team name */
	"brodrig4",
	/* First member's full name */
	"Bienvenido Rodriguez",
	/* First member's email address */
	"brodrig4@binghamton.edu",
	/* Second member's full name (leave blank if none) */
	"",
	/* Second member's email address (leave blank if none) */
	""
};
/* Code from page 830 in the text */
/* Basic Constants and Macros */
#define WSIZE 4 // Word and header/footer size (bytes) 
#define DSIZE 8 // Double word size (bytes)
#define CHUNKSIZE 16 // initial heap size (bytes)
#define MINIMUM 24 // Min block size

#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) > (y) ? (x) : (y))
/* single word (4) or double word (8) alignment */
#define ALIGNMENT 8

/* rounds up to the nearest multiple of ALIGNMENT */
#define ALIGN(p) (((size_t)(p) + (ALIGNMENT-1)) & ~0x7)

/* Pack a size and allocated bit into a word */
#define EncodeSize(size, alloc) ((size) | (alloc))

/* Read and write a word at address p */
#define ReadValAt(p) (*(int *)(p))
#define WriteValAt(p, val) (*(int *)(p) = (val)) //unsigned?

/*Read and size the allocated fields from address p */
#define ReadBlockSize(p) (ReadValAt(p) & ~0x7)
#define ReadBlockState(p) (ReadValAt(p) & 0x1)

/*Given block ptr bp, compute address of next and previous blocks */
#define NextBlock(bp)  ((void *)(bp) + ReadBlockSize(GetBlockHead(bp)))
#define PrevBlock(bp)  ((void *)(bp) - ReadBlockSize(GetBlockHead(bp) - WSIZE))

/* Given block ptr bp, compute address of its header and footer */
#define GetBlockHead(bp)       ((void *)(bp) - WSIZE)
#define GetBlockFoot(bp)       ((void *)(bp) + ReadBlockSize(GetBlockHead(bp)) - DSIZE)

/* Given block ptr, compute address of next and previous FREE blocks */
#define NextFree(bp) (*(void **)(bp + DSIZE))
#define PrevFree(bp) (*(void **)(bp))

#define GetParentFree(bp) (*(void **)(bp + DSIZE + DSIZE)
#define GetLeftFree(bp) (*(void **)(bp + DSIZE))
#define GetRightFree(bp) (*(void **)(bp))

#define SIZE_T_SIZE (ALIGN(sizeof(size_t)))

/* Global variable that our code will use to manage its linked lists */
static void *heapBlockPtr = 0; // This is a pointer to the first block
static void *heapFreePtr = 0; // This is a pointer to the first free block

/* Here we define the function prototypes that assist with the program */
static void *extendHeap(size_t word);
static void *findSpace(size_t adjustedSize);
static void *coalesce(void *blockPtr);
static void insertToFreeList(void *bp);
static void removeBlock(void *bp);
static void place(void *bp, size_t asize);

/* 
 * mm_init - initialize the malloc package.
 *
 * Begin by setting up our "Free" linked list. The linked list will use a "dummy"
 * header to denote its end for simplicity. It's header will be set as "ALLOC'd"
 * for easy differentiation between free nodes and the end of the list.
 */
int mm_init(void)
{
	/*return -1 if unable to get heap space*/
	if ((heapBlockPtr = mem_sbrk(2*MINIMUM)) == NULL)
	{
		return -1;
	}
	WriteValAt(heapBlockPtr, 0); //Alignment padding

	/*initialize dummy block header*/
	WriteValAt(heapBlockPtr + WSIZE, EncodeSize(MINIMUM, 1)); //WSIZE = padding
	WriteValAt(heapBlockPtr + DSIZE, 0); //PREV pointer
	WriteValAt(heapBlockPtr + DSIZE+WSIZE, 0); //NEXT pointer
	WriteValAt(heapBlockPtr + DSIZE+DSIZE, 0); //PARENT pointer

	/*initialize dummy block footer*/
	WriteValAt(heapBlockPtr + MINIMUM, EncodeSize(MINIMUM, 1)); 

	/*initialize dummy tail block*/
	WriteValAt(heapBlockPtr+WSIZE + MINIMUM, EncodeSize(0, 1)); 

	/*initialize the free list pointer to the tail block*/
	heapFreePtr = heapBlockPtr + DSIZE;

	return 0;
}

/* 
* mm_malloc - Allocate a block by incrementing the brk pointer.
*     Always allocate a block whose size is a multiple of the alignment.
*/
void *mm_malloc(size_t size)
{
	size_t asize;      /* adjusted block size */
	size_t extendsize; /* amount to extend heap if no fit */
	char *bp;

	/* Ignore spurious requests */
	if (size <= 0)
		return NULL;

	/* Adjust block size to include overhead and alignment reqs */
	asize = MAX(ALIGN(size) + DSIZE, MINIMUM);

	/* Search the free list for a fit */
	if ((bp = findSpace(asize))) 
	{
		place(bp, asize);
		return bp;
	}
	/* No fit found. Get more memory and place the block */
	extendsize = MAX(asize, CHUNKSIZE);
	//return NULL if unable to get heap space
	if ((bp = extendHeap(extendsize/WSIZE)) == NULL) 
		return NULL;
	place(bp, asize);
	return bp;
}

/*
* mm_free - Freeing a block does nothing.
*/
 void  mm_free(void *bp)
{
	size_t size = ReadBlockSize(GetBlockHead(bp));

	//set header and footer to unallocated
	WriteValAt(GetBlockHead(bp), EncodeSize(size, 0)); 
	WriteValAt(GetBlockFoot(bp), EncodeSize(size, 0));
	coalesce(bp); //coalesce and add the block to the free list
}

/*
* mm_realloc - Implemented simply in terms of mm_malloc and mm_free
*/
void *mm_realloc(void *ptr, size_t size)
{
	if(size <= 0)
	{
		free(ptr);
		return 0;
	}

	/* If oldptr is NULL, then this is just malloc. */
	if(ptr == NULL)
	{
		return mm_malloc(size);
	}

	void *newptr;
	size_t newSize = MAX(ALIGN(size) + DSIZE, MINIMUM);
	size_t oldSize = ReadBlockSize(GetBlockHead(ptr));
	size_t copySize = MIN(newSize, oldSize);
	size_t nextSize = 0;
	int isNextFree = !ReadBlockState(GetBlockHead(NextBlock(ptr)));

	if (isNextFree)
	{
		nextSize = ReadBlockSize(GetBlockHead(NextBlock(ptr)));
	}

	if (newSize <= oldSize + nextSize)
	{
		if (isNextFree)
		{
			removeBlock(NextBlock(ptr));
		}
		newptr = ptr;
		if (oldSize + nextSize - newSize > MINIMUM)
		{
			WriteValAt(GetBlockHead(newptr), EncodeSize(newSize, 1));
			WriteValAt(GetBlockFoot(newptr), EncodeSize(newSize, 1));
			WriteValAt(GetBlockHead(NextBlock(newptr)), EncodeSize(oldSize+nextSize-newSize, 0));
			mm_free(NextBlock(newptr));
		} else {
			newSize = oldSize + nextSize;
			WriteValAt(GetBlockHead(newptr), EncodeSize(newSize, 1));
			WriteValAt(GetBlockFoot(newptr), EncodeSize(newSize, 1));
		}
	} else {
		newptr = mm_malloc(newSize);
		if (newptr == NULL)
			return NULL;
		memcpy(newptr, ptr, copySize);
		mm_free(ptr);
	}

	return newptr;
}

static void *coalesce(void *bp)
{
	size_t prev_alloc = 1;
	if (PrevBlock(bp) == bp)
	{
		prev_alloc = 1;
	} else {
		prev_alloc = ReadBlockState(GetBlockFoot(PrevBlock(bp)));
	}
	size_t next_alloc = 1;
	if (NextBlock(bp))
	{
		next_alloc = ReadBlockState(GetBlockHead(NextBlock(bp)));
	}
	size_t size = ReadBlockSize(GetBlockHead(bp));

	/* Case 1, extend the block leftward */
	if (prev_alloc && !next_alloc) 
	{
		removeBlock(NextBlock(bp));
		size += ReadBlockSize(GetBlockHead(NextBlock(bp)));
		WriteValAt(GetBlockHead(bp), EncodeSize(size, 0));
		WriteValAt(GetBlockFoot(bp), EncodeSize(size, 0));
	}

	/* Case 2, extend the block rightward */
	else if (!prev_alloc && next_alloc) 
	{
		removeBlock(PrevBlock(bp));
		size += ReadBlockSize(GetBlockHead(PrevBlock(bp)));
		WriteValAt(GetBlockFoot(bp), EncodeSize(size, 0));
		WriteValAt(GetBlockHead(PrevBlock(bp)), EncodeSize(size, 0));
		bp = PrevBlock(bp);
	}

	/* Case 3, extend the block in both directions */
	else if (!prev_alloc && !next_alloc) 
	{
		removeBlock(PrevBlock(bp));
		removeBlock(NextBlock(bp));
		size += ReadBlockSize(GetBlockHead(PrevBlock(bp))) + 
			ReadBlockSize(GetBlockHead(NextBlock(bp)));
		WriteValAt(GetBlockHead(PrevBlock(bp)), EncodeSize(size, 0));
		WriteValAt(GetBlockFoot(NextBlock(bp)), EncodeSize(size, 0));
		bp = PrevBlock(bp);
	}

	insertToFreeList(bp);
	return bp;
}
/* Book Implementation of ExtendHeap */
static void *extendHeap(size_t words)
{
	void *bp;
	size_t size; 

	/* Allocate an even number of words to maintain alignment */
	size = (words % 2) ? (words + 1) * WSIZE : words *WSIZE;
	if (size < MINIMUM)
		size = MINIMUM;
	if ((long)(bp = mem_sbrk(size)) == -1)
	{
		return NULL;
	}

	/* Initialize free block header/footer and the epilogue header */
	WriteValAt(GetBlockHead(bp), EncodeSize(size, 0)); /* Free block header */
	WriteValAt(GetBlockFoot(bp), EncodeSize(size, 0)); /* Free block footer */
	WriteValAt(GetBlockHead(NextBlock(bp)), EncodeSize(0, 1)); /* New Epilogue header */

	/* Coalesce if the previous block was free */
	return coalesce(bp);
}

static void *findSpace(size_t adjustedSize)
{
	// Since our linked list is sorted from most space to least space,
	// we can instantly quit once we see that the first node can't fit
	// our request.

	void *blockPtr;
	blockPtr = heapFreePtr;

	if (ReadBlockState(GetBlockHead(blockPtr)) == 1)
		return NULL;

	if (adjustedSize <= (size_t)ReadBlockSize(GetBlockHead(blockPtr)))
		return blockPtr;

	return NULL;
}

static void insertToFreeList(void *bp)
{
//	To optomize time... we'll put the list into an ascending linked list.
	void *newPos = heapFreePtr;
	size_t state = 0;
	size_t size = ReadBlockSize(GetBlockHead(bp));
	while ((state = ReadBlockState(GetBlockHead(newPos))) == 0)
	{
		if (size >= (size_t)ReadBlockSize(GetBlockHead(newPos)))
		{
			break;
		}
		newPos = NextFree(newPos);
	}

	if (newPos == heapFreePtr)
	{
		NextFree(bp) = heapFreePtr; //Sets next ptr to start of free list
		PrevFree(bp) = NULL; // Sets prev pointer to NULL
		PrevFree(heapFreePtr) = bp; //Sets current's prev to new block
		heapFreePtr = bp; // Sets start of free list as new block	
	} else {
		NextFree(bp) = newPos;
		PrevFree(bp) = PrevFree(newPos);
		NextFree(PrevFree(bp)) = bp;
		PrevFree(NextFree(bp)) = bp;
	}
}


static void removeBlock(void *bp)
{
	// If there's a previous block, set its next pointer to the 
	// next block.
	// If not, set the block's previous pointer to the prev block/
	if (PrevFree(bp))
	{
		NextFree(PrevFree(bp)) = NextFree(bp);
	} else {
		heapFreePtr = NextFree(bp);
	}

	PrevFree(NextFree(bp)) = PrevFree(bp);
}


static void place(void *bp, size_t asize)
{
	/* Gets the size of the whole free block */
	size_t csize = ReadBlockSize(GetBlockHead(bp));

	/* If the difference is at least 24 bytes, change the header and footer
	* info for the allocated block (size = asize, allocated = 1) and
	* remove the block from the free list.
	* Then, split the block by:
	* (1) Changing the header and footer info for the free block created from the
	* remaining space (size = csize-asize, allocated = 0)
	* (2) Coalescing the new free block with adjacent free blocks
	*/
	if ((csize - asize) >= MINIMUM)
	{
		WriteValAt(GetBlockHead(bp), EncodeSize(asize, 1));
		WriteValAt(GetBlockFoot(bp), EncodeSize(asize, 1));
		removeBlock(bp);
		bp = NextBlock(bp);
		WriteValAt(GetBlockHead(bp), EncodeSize(csize-asize, 0));
		WriteValAt(GetBlockFoot(bp), EncodeSize(csize-asize, 0));
		coalesce(bp);
	} else {
		WriteValAt(GetBlockHead(bp), EncodeSize(csize, 1));
		WriteValAt(GetBlockFoot(bp), EncodeSize(csize, 1));
		removeBlock(bp);
	}
}
