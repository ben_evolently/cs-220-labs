/********************************************************
 * Kernels to be optimized for the CS:APP Performance Lab
 ********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "defs.h"

/* 
 * Please fill in the following team struct 
 */
team_t team = {
    "jwallc2_brodrig4",              /* Team name */

    "Bienvenido Rodriguez",     /* First member full name */
    "brodrig4@binghamton.edu",  /* First member email address */

    "Joanna Wallace",                   /* Second member full name (leave blank if none) */
    "jwallac2@binghamton.edu"                    /* Second member email addr (leave blank if none) */
};

/***************
 * ROTATE KERNEL
 ***************/

/******************************************************
 * Your different versions of the rotate kernel go here
 ******************************************************/

/* 
 * naive_rotate - The naive baseline version of rotate 
 */
char naive_rotate_descr[] = "naive_rotate: Naive baseline implementation";
void naive_rotate(int dim, pixel *src, pixel *dst) 
{
    int i, j;

    for (i = 0; i < dim; i++)
	for (j = 0; j < dim; j++)
	    dst[RIDX(dim-1-j, i, dim)] = src[RIDX(i, j, dim)];
}

/* 
 * rotate - Your current working version of rotate
 * IMPORTANT: This is the version you will be graded on
 */
char rotate_descr[] = "rotate: Current working version";
void rotate(int dim, pixel *src, pixel *dst) 
{
		//found that blocking by 16 was faster than by 8, but the same at 32
    //blocking helps maximize cache access
    int i, j, i1, j1;
        for (i1 = 0; i1 < dim; i1 += 16)
    	    for (j1 = 0; j1 < dim; j1 += 16)
	        for(i = i1; i < i1 + 16; i++)
                    for (j = j1; j < j1 + 16; j++)
                        dst[RIDX(dim-1-j, i, dim)] = src[RIDX(i, j, dim)]; //same as naive version
}

/*********************************************************************
 * register_rotate_functions - Register all of your different versions
 *     of the rotate kernel with the driver by calling the
 *     add_rotate_function() for each test function. When you run the
 *     driver program, it will test and report the performance of each
 *     registered test function.  
 *********************************************************************/

void register_rotate_functions() 
{
    add_rotate_function(&naive_rotate, naive_rotate_descr);   
    add_rotate_function(&rotate, rotate_descr);   
    /* ... Register additional test functions here */
}


/***************
 * SMOOTH KERNEL
 **************/

/***************************************************************
 * Various typedefs and helper functions for the smooth function
 * You may modify these any way you like.
 **************************************************************/

/* A struct used to compute averaged pixel value */
typedef struct {
    int red;
    int green;
    int blue;
    int num;
} pixel_sum;

/* Compute min and max of two integers, respectively */
static int min(int a, int b) { return (a < b ? a : b); }
static int max(int a, int b) { return (a > b ? a : b); }

/* 
 * initialize_pixel_sum - Initializes all fields of sum to 0 
 */
static void initialize_pixel_sum(pixel_sum *sum) 
{
    sum->red = sum->green = sum->blue = 0;
    sum->num = 0;
    return;
}

/* 
 * accumulate_sum - Accumulates field values of p in corresponding 
 * fields of sum 
 */
static void accumulate_sum(pixel_sum *sum, pixel p) 
{
    sum->red += (int) p.red;
    sum->green += (int) p.green;
    sum->blue += (int) p.blue;
    sum->num++;
    return;
}

/* 
 * assign_sum_to_pixel - Computes averaged pixel value in current_pixel 
 */
static void assign_sum_to_pixel(pixel *current_pixel, pixel_sum sum) 
{
    current_pixel->red = (unsigned short) (sum.red/sum.num);
    current_pixel->green = (unsigned short) (sum.green/sum.num);
    current_pixel->blue = (unsigned short) (sum.blue/sum.num);
    return;
}

/* 
 * avg - Returns averaged pixel value at (i,j) 
 */
static pixel avg(int dim, int i, int j, pixel *src) 
{
    int ii, jj;
    pixel_sum sum;
    pixel current_pixel;

    initialize_pixel_sum(&sum);
    for(ii = max(i-1, 0); ii <= min(i+1, dim-1); ii++) 
	for(jj = max(j-1, 0); jj <= min(j+1, dim-1); jj++) 
	    accumulate_sum(&sum, src[RIDX(ii, jj, dim)]);

    assign_sum_to_pixel(&current_pixel, sum);
    return current_pixel;
}

// New function to get average in which we use to execute against the outer bounds
// In the optimized version of smooth
// Ie, We check the furthest left column, the top row, the bottom row and the right
// column
static pixel getNewAverage(int dim, int i, int j, pixel *src)
{
  int x; 
	int y;
	int a; 
	int b;  
	int c;  
	int d; 
  pixel_sum sum;
  pixel cPixel;

  sum.red = 0;
	sum.green = 0;
	sum.blue = 0;
	sum.num = 0;

	if ((i - 1) > 0)
	{
		a = i - 1;
	} else {
		a = 0;
	}

	if ((i + 1) < (dim - 1))
	{
		c = i + 1;
	} else {
		c = dim - 1;
	}

	if ((j - 1) > 0)
	{
		b = j - 1;
	} else {
		b = 0;
	}

	if((j + 1) < (dim - 1))
	{
		d = j + 1;
	} else {
		d = dim - 1;
	}

  for (x = a; x <= c; x++)
	{
		for (y = b; y <= d; y++)
      {
				sum.red += (int) src[RIDX(x, y, dim)].red;
				sum.green += (int) src[RIDX(x, y, dim)].green;
        sum.blue += (int) src[RIDX(x, y, dim)].blue;
				sum.num++;
      }
	}
  cPixel.red = (unsigned short) (sum.red/sum.num);
  cPixel.green = (unsigned short) (sum.green/sum.num);
  cPixel.blue = (unsigned short) (sum.blue/sum.num);

  return cPixel;
}


/******************************************************
 * Your different versions of the smooth kernel go here
 ******************************************************/

/*
 * naive_smooth - The naive baseline version of smooth 
 */
char naive_smooth_descr[] = "naive_smooth: Naive baseline implementation";
void naive_smooth(int dim, pixel *src, pixel *dst) 
{
    int i, j;

    for (i = 0; i < dim; i++)
	for (j = 0; j < dim; j++)
	    dst[RIDX(i, j, dim)] = avg(dim, i, j, src);
}

/*
 * smooth - Your current working version of smooth. 
 * IMPORTANT: This is the version you will be graded on
 */
char smooth_descr[] = "smooth: Current working version";
void smooth(int dim, pixel *src, pixel *dst) 
{
  int i;
  int j;

  // Top-left corner 
  dst[RIDX(0, 0, dim)].red = (unsigned short) ((src[RIDX(0, 0, dim)].red + src[RIDX(0, 1, dim)].red + src[RIDX(1, 0, dim)].red + src[RIDX(1, 1, dim)].red)/4);

  dst[RIDX(0, 0, dim)].green = (unsigned short) ((src[RIDX(0, 0, dim)].green + src[RIDX(0, 1, dim)].green + src[RIDX(1, 0, dim)].green + src[RIDX(1, 1, dim)].green)/4);

	dst[RIDX(0, 0, dim)].blue = (unsigned short) ((src[RIDX(0, 0, dim)].blue + src[RIDX(0, 1, dim)].blue + src[RIDX(1, 0, dim)].blue + src[RIDX(1, 1, dim)].blue)/4);
  // End top left corner 

  // Top-Right corner
  dst[RIDX(dim - 1, 0, dim)].red = (unsigned short) 
    ((src[RIDX(dim - 1, 0, dim)].red + src[RIDX(dim - 2, 0, dim)].red + src[RIDX(dim - 1, 1, dim)].red + src[RIDX(dim - 2, 1, dim)].red)/4);
  
  dst[RIDX(dim - 1, 0, dim)].green = (unsigned short) 
    ((src[RIDX(dim - 1, 0, dim)].green + src[RIDX(dim - 2, 0, dim)].green + src[RIDX(dim - 1, 1, dim)].green + src[RIDX(dim - 2, 1, dim)].green)/4);

	dst[RIDX(dim - 1, 0, dim)].blue = (unsigned short) 
    ((src[RIDX(dim - 1, 0, dim)].blue + src[RIDX(dim - 2, 0, dim)].blue + src[RIDX(dim - 1, 1, dim)].blue + src[RIDX(dim - 2, 1, dim)].blue)/4);
  // End top-right corner 

  // Bottom-left corner 
  dst[RIDX(0, dim - 1, dim)].red = (unsigned short) 
    ((src[RIDX(0, dim - 1, dim)].red + src[RIDX(0, dim - 2, dim)].red + src[RIDX(1, dim - 1, dim)].red + src[RIDX(1, dim - 2, dim)].red)/4);

  dst[RIDX(0, dim - 1, dim)].green = (unsigned short) 
    ((src[RIDX(0, dim - 1, dim)].green + src[RIDX(0, dim - 2, dim)].green + src[RIDX(1, dim - 1, dim)].green + src[RIDX(1, dim - 2, dim)].green)/4);

	dst[RIDX(0, dim - 1, dim)].blue = (unsigned short) 
    ((src[RIDX(0, dim - 1, dim)].blue + src[RIDX(0, dim - 2, dim)].blue + src[RIDX(1, dim - 1, dim)].blue + src[RIDX(1, dim - 2, dim)].blue)/4);
  // End bottom-left corner 

  // Bottom-right corner
  dst[RIDX(dim - 1, dim - 1, dim)].red = (unsigned short) 
    ((src[RIDX(dim - 1, dim - 1, dim)].red + src[RIDX(dim - 2, dim - 1, dim)].red + src[RIDX(dim - 1, dim - 2, dim)].red + src[RIDX(dim - 2, dim - 2, dim)].red)/4);
  
  dst[RIDX(dim - 1, dim - 1, dim)].green = (unsigned short) 
    ((src[RIDX(dim - 1, dim - 1, dim)].green + src[RIDX(dim - 2, dim - 1, dim)].green + src[RIDX(dim - 1, dim - 2, dim)].green + src[RIDX(dim - 2, dim - 2, dim)].green)/4);

	dst[RIDX(dim - 1, dim - 1, dim)].blue = (unsigned short) 
    ((src[RIDX(dim - 1, dim - 1, dim)].blue + src[RIDX(dim - 2, dim - 1, dim)].blue + src[RIDX(dim - 1, dim - 2, dim)].blue + src[RIDX(dim - 2, dim - 2, dim)].blue)/4);
  /* End bottom-right corner */


  // Here we execute and check first column on the left (column 0)
  for (i = 1; i < dim - 1; i++)
  {  
		dst[RIDX(0, i, dim)] = getNewAverage(dim, 0, i, src);
	}
	// Here we execute and check the column on the right (column dim - 1) */
  for (i = 1; i < dim - 1; i++)
	{
    dst[RIDX(dim - 1, i, dim)] = getNewAverage(dim, dim - 1, i, src);
	}
  // Here we execute and check for the first row (row 0)
  for (i = 1; i < dim - 1; i++)
  {  
		dst[RIDX(i, 0, dim)] = getNewAverage(dim, i, 0, src);
	}
	//Here we execute and check the bottom row (row dim - 1) */
  for (i = 1; i < dim - 1; i++)
  {  
		dst[RIDX(i, dim - 1, dim)] = getNewAverage(dim, i, dim - 1, src);
	}


  // Here we execute the inner elements of the matrix
  for (i = 1; i < dim - 1; i++)
	{
    for (j = 1; j < dim - 1; j++)
      {

				// Red
				dst[RIDX(i, j, dim)].red = (unsigned short) ((src[RIDX(i - 1, j - 1, dim)].red + 
				src[RIDX(i, j - 1, dim)].red + src[RIDX(i + 1, j - 1, dim)].red + src[RIDX(i - 1, j, dim)].red +
				src[RIDX(i, j, dim)].red + src[RIDX(i + 1, j, dim)].red + src[RIDX(i - 1, j + 1, dim)].red +
				src[RIDX(i, j + 1, dim)].red + src[RIDX(i + 1, j + 1, dim)].red)/9);
				// End Red
				
				//Green
				dst[RIDX(i, j, dim)].green = (unsigned short) ((src[RIDX(i - 1, j - 1, dim)].green +
				src[RIDX(i, j - 1, dim)].green + src[RIDX(i + 1, j - 1, dim)].green + src[RIDX(i - 1, j, dim)].green +
				src[RIDX(i, j, dim)].green + src[RIDX(i + 1, j, dim)].green + src[RIDX(i - 1, j + 1, dim)].green +
				src[RIDX(i, j + 1, dim)].green + src[RIDX(i + 1, j + 1, dim)].green)/9);
				//End Green

				//Blue
        dst[RIDX(i, j, dim)].blue = (unsigned short) ((src[RIDX(i - 1, j - 1, dim)].blue +
				src[RIDX(i, j - 1, dim)].blue + src[RIDX(i + 1, j - 1, dim)].blue + src[RIDX(i - 1, j, dim)].blue +
				src[RIDX(i, j, dim)].blue + src[RIDX(i + 1, j, dim)].blue + src[RIDX(i - 1, j + 1, dim)].blue +
				src[RIDX(i, j + 1, dim)].blue + src[RIDX(i + 1, j + 1, dim)].blue)/9);
				//End Blue
      }
	}
}


/********************************************************************* 
 * register_smooth_functions - Register all of your different versions
 *     of the smooth kernel with the driver by calling the
 *     add_smooth_function() for each test function.  When you run the
 *     driver program, it will test and report the performance of each
 *     registered test function.  
 *********************************************************************/

void register_smooth_functions() {
    add_smooth_function(&smooth, smooth_descr);
    add_smooth_function(&naive_smooth, naive_smooth_descr);
    /* ... Register additional test functions here */
}

